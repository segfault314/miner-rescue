function createTestMap()
  -- Test function for creating a test map
  -- This is meant to be used with the test grid function
  
  for i=0,259 do
    
    local maptile = {}
    maptile.type = ""
    maptile.posy = math.floor(i / 26)
    maptile.posx = i - (maptile.posy * 26)
    maptile.seed = math.random(1,100)
    maptile.travelcost = 1
    maptile.cantravel = 0
    
    if maptile.seed < 83 then
      maptile.type = terrainTypes.blank
    end
    if maptile.seed >= 83 and maptile.seed < 93 then
      maptile.type = terrainTypes.shoal
      maptile.travelcost = 2
    end
    if maptile.seed >= 93 and maptile.seed < 99 then
      maptile.type = terrainTypes.dust
    end
    if maptile.seed >= 99 then
      maptile.type = terrainTypes.wreck
      maptile.travelcost = 2
    end
    
    table.insert(testmap,maptile)
  end
  
end

function findTestMapMoveLimit()
  -- Function for figuring out how far the player can travel
  -- This is meant to be used with the test grid function
  
  for i,t in ipairs(testmap) do
    local diffx = math.abs(t.posx - player.posx)
    local diffy = math.abs(t.posy - player.posy)
    
    if (diffx + diffy) <= 4 then
      t.cantravel = 1
    else
      t.cantravel = 0
    end
  end
  
end

function findMouseTileLocation()
  -- Function for figuring out which tile the mouse is hovering over
  -- This is meant to be used with the test grid function
  
  mouselocation.x = math.floor((love.mouse.getX() - 16) / 48)
  mouselocation.y = math.floor((love.mouse.getY() - 16) / 48)
  
end

function movePlayerToTarget()
  -- Function for moving the player to a tile if they're within range
  -- This is meant to be used with the test grid function
  
  tile = mouselocation.x + (mouselocation.y * 26)
  
  if testmap[tile + 1].cantravel == 1 then
    player.posx = testmap[tile + 1].posx
    player.posy = testmap[tile + 1].posy
  end
end