player = {}
player.posx = 8
player.posy = 8
player.isFacingRight = 0

function player.init()
  player.ships = {
    Ship.new({class = "destroyer", hp = 8}, 22, 2),
    Ship.new({class = "destroyer", hp = 8}, 21, 3),
    Ship.new({class = "frigate"  , hp = 8}, 22, 6),
    Ship.new({class = "carrier"  , hp = 8}, 23, 7),
    Ship.new({class = "fighters" , hp = 8}, 22, 8)
  }
end

function player.draw_ships()
  for k,v in ipairs(player.ships) do
    v:draw("player")
  end
end