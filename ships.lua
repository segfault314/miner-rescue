--Base class for Ships
Ship = {}
Ship.__index = Ship
setmetatable(Ship, {
    __call = function (cls, ...)
      return cls.new(...)
    end,
  })

--[[
  Ship Reference:
  class = the type of ship (destroyer, frigate, carrier)
  mhp   = maximum hitpoints
  hp    = current hitpoints
  speed = the number of tiles the ship can move in one turn
  pos   = x and y position
  guns  = the weapons that are mounted to the ship
--]]
function Ship.new(stat_table, x, y, flags)
  local self = setmetatable({}, Ship)
  local speeds = {
    destroyer = 4,
    frigate   = 3,
    carrier   = 2,
    fighters  = 4,
    torpedo   = 6
  }
  
  self.class = stat_table.class
  self.hp    = stat_table.hp
  self.mhp   = self.hp
  self.speed = speeds[self.class]
  self.pos   = {x = x, y = y}
  self.guns  = {
    
  }
  
  return self
end

function Ship:draw(owner)
  
  if owner == "player" then
    love.graphics.draw(images.playerships, playershipTypes[self.class], ((self.pos.x * 48) + 16), ((self.pos.y * 48) + 16))
    love.graphics.draw(images.grid, gridTypes.ally, ((self.pos.x * 48) + 16), ((self.pos.y * 48) + 16))
  elseif owner == "enemy" then
    love.graphics.draw(images.enemyships, enemyshipTypes[self.class], ((self.pos.x * 48) + 16), ((self.pos.y * 48) + 16))
    love.graphics.draw(images.grid, gridTypes.enemy, ((self.pos.x * 48) + 16), ((self.pos.y * 48) + 16))
  end
  
  return
end