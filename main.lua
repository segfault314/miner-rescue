function love.load()
  math.randomseed(os.time())
  
  love.window.setMode(1280, 720)
  textfont = love.graphics.newFont("CamingoCode-v1.0/CamingoCode-Regular.ttf", 14, mono)
  
  -- set the color (nice dark burgundy)
  love.graphics.setBackgroundColor(0.09, 0.03, 0.04)
  
  -- graphics lua file for instantiating graphics objects
  require('graphics')
  
  -- ships lua file containing the ship object metatable and metatable functions
  require('ships')
  
  -- player actor lua file for keeping track of player actors
  require('player')
  
  -- drawing lua file for actually drawing graphics to the screen
  require('drawing')
  
  -- test map file for functions related to generating the test map
  require('testmap')
  
  player.init()
  
  testmap = {}
  createTestMap()
  
  mouselocation = {}
  
end

function love.update(dt)
  findTestMapMoveLimit()
  findMouseTileLocation()
end

function love.draw()
  drawTestGrid()
  drawTestMap()
  drawTestPlayer()
  drawTestMouseHighlight()
  drawTestMouseLine()
  drawTestFrame()
  drawTestMoveLimit()
end

function love.keypressed(key, scancode, isrepeat)
  
  if key == "w" and isrepeat == false and player.posy > 0 then
    player.posy = player.posy - 1
  elseif key == "s" and isrepeat == false and player.posy < 9 then
    player.posy = player.posy + 1
  elseif key == "a" and isrepeat == false and player.posx > 0 then
    player.posx = player.posx - 1
    player.isFacingRight = 0
  elseif key == "d" and isrepeat == false and player.posx < 25 then
    player.posx = player.posx + 1
    player.isFacingRight = 1
  end
  
end

function love.mousepressed(x, y, button, istouch)
  if button == 1 then
    movePlayerToTarget()
  end
end