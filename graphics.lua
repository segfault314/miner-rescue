images = {}
images.grid = love.graphics.newImage('gfx/grid.png')
images.terrain = love.graphics.newImage('gfx/terrain.png')
images.weapons = love.graphics.newImage('gfx/wp_types.png')
images.playerships = love.graphics.newImage('gfx/player_ships.png')
images.enemyships = love.graphics.newImage('gfx/enemy_ships.png')

gridTypes = {}
gridTypes.normal = love.graphics.newQuad(0, 0, 48, 48, images.grid:getDimensions())
gridTypes.hazard = love.graphics.newQuad(48, 0, 48, 48, images.grid:getDimensions())
gridTypes.enemy  = love.graphics.newQuad(96, 0, 48, 48, images.grid:getDimensions())
gridTypes.ally   = love.graphics.newQuad(144, 0, 48, 48, images.grid:getDimensions())

terrainTypes = {}
terrainTypes.blank = love.graphics.newQuad(0, 0, 48, 48, images.terrain:getDimensions())
terrainTypes.shoal = love.graphics.newQuad(48, 0, 48, 48, images.terrain:getDimensions())
terrainTypes.wreck = love.graphics.newQuad(96, 0, 48, 48, images.terrain:getDimensions())
terrainTypes.dust  = love.graphics.newQuad(144, 0, 48, 48, images.terrain:getDimensions())

playershipTypes = {}
playershipTypes.destroyer = love.graphics.newQuad(0, 0, 48, 48, images.playerships:getDimensions())
playershipTypes.frigate = love.graphics.newQuad(48, 0, 48, 48, images.playerships:getDimensions())
playershipTypes.carrier = love.graphics.newQuad(96, 0, 48, 48, images.playerships:getDimensions())
playershipTypes.fighters = love.graphics.newQuad(144, 0, 48, 48, images.playerships:getDimensions())
playershipTypes.torpedo = love.graphics.newQuad(192, 0, 48, 48, images.playerships:getDimensions())

enemyshipTypes = {}
enemyshipTypes.destroyer = love.graphics.newQuad(0, 0, 48, 48, images.enemyships:getDimensions())
enemyshipTypes.frigate = love.graphics.newQuad(48, 0, 48, 48, images.enemyships:getDimensions())
enemyshipTypes.carrier = love.graphics.newQuad(96, 0, 48, 48, images.enemyships:getDimensions())
enemyshipTypes.fighters = love.graphics.newQuad(144, 0, 48, 48, images.enemyships:getDimensions())
enemyshipTypes.torpedo = love.graphics.newQuad(192, 0, 48, 48, images.enemyships:getDimensions())